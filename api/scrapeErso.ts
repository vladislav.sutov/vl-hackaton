import { kv } from '@vercel/kv';
import { VercelRequest, VercelResponse } from '@vercel/node';
import puppeteer from 'puppeteer';

export default async function handler(request: VercelRequest, response: VercelResponse) {
	try {
		const browser = await puppeteer.launch();
		const page = await browser.newPage();
		await page.goto('https://erso.tv/landing/br/pb_kPgRUjZ/tU73plz0J0', {
			waitUntil: 'networkidle0'
		});

		const data = await page.$$eval(
			'ul.multilister.small-block-grid-1.medium-block-grid-2 li',
			(lis) => {
				return lis.map((li) => {
					const title = li.querySelector('div.element-data-txt-name')?.textContent.trim();
					const imgElement = li.querySelector('div.element-data-img-hldr img');
					const img = imgElement ? imgElement.src : '';
					const venue = 'erso.tv';
					const datetime = [
						li.querySelector('div.element-time-time')?.textContent.trim(),
						li.querySelector('div.element-time-day')?.textContent.trim(),
						li.querySelector('div.element-time-year')?.textContent.trim()
					]
						.filter(Boolean)
						.join(' ');
					const urlElement = li.querySelector('a');
					const url = urlElement ? urlElement.href : '';

					const event = { title, img, venue, datetime, url };

					return event;
				});
			}
		);

		const pipeline = kv.pipeline();

		for (const event of data) {
			const key = event.url.split('/').reverse()[0];
			pipeline.hset(`event:${key}`, event);
		}

		await pipeline.exec();
		await browser.close();

		return response.status(200);
	} catch (error) {
		console.error(`Could not scrape the website: ${error}`);
	}
}
