import type { VercelRequest, VercelResponse } from '@vercel/node';
import { kv } from '@vercel/kv';

export default async function handler(request: VercelRequest, response: VercelResponse) {
	const eventKeys = await kv.keys('*');
	const pipeline = kv.pipeline();

	for (const key of eventKeys) {
		pipeline.hgetall(key);
	}

	const res = await pipeline.exec();

	return response.status(200).json(res);
}
